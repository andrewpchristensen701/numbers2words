var destination = document.getElementById("div");

var units = ["", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"];
var teens = ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"];
var tens = ["", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety"];
function createAnElement(text){

    let newElement = document.createElement('div');
    let textNode = document.createTextNode(text);
    newElement.appendChild(textNode);
    destination.appendChild(newElement);
}

function numberstowords() {
    for (let i = 0; i <= 1000; i++) {

        if (i < 10) {
            createAnElement(units[i])
        }

        else if (i > 9 && i < 20) {
            createAnElement(teens[i - 10])
        }

        else if (i > 19 && i <= 99) {
            let digits = String(i)
            let digit1 = Number(digits[0]);
            let digit2 = Number(digits[1]);

            let tensplace = tens[digit1 - 1] + units[digit2]
            createAnElement(tensplace)
        }
        else if (i > 99 && i <= 999) {
            let digits = String(i)
            let digit1 = Number(digits[0]);
            let digit2 = Number(digits[1]);
            let digit3 = Number(digits[2]);

            let hundreds = ''

            if (digit2 === 1) {
                hundreds = units[digit1] + "hundred" + teens[digit3]
            }
            else {
                if (digit2 === 0) {
                    hundreds = units[digit1] + "hundred" + units[digit3]
                }
                else {
                    hundreds = units[digit1] + "hundred" + tens[digit2 - 1] + units[digit3]
                }
            }
            createAnElement(hundreds)
        }
        else{
            let thousand = 'thousand'

        
            createAnElement(thousand)
        }

    }


}
numberstowords()
